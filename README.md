It is easy for UC Davis students to find the right apartment with just the floor plan with Davisville Management Company. With our three distinctive apartments complexes located an easy bicycle ride from campus, Aggie Square Apartments, Almondwood Apartments, and Fountain Circle Townhomes. 

Address: 2525 2nd St, Davis, CA 95618, USA

Phone: 530-758-5800

Website: https://www.davisapartmentsforrent.com
